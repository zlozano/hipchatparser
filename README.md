# HipChat Parser #

### Design ###

The REST API was created using a Spring Rest Controller. There is only one exposed API:

`POST /api/v1/parse`

This endpoint returns a 200 OK with a JSON payload as described in the spec.

Since raw unstructured text is the input, I decided on a POST request (rather than a GET with encoded parameters).  The methods returns a 200 rather than a 201 since no resource is actually being created.

The business logic is written using a template pattern.  Each parser extends an abstract class and implements the abstract method, providing some specialized functionality (e.g. a specific regex). This pattern helps satisfy both the Single Responsibility Principle and the Open-Close Principle.

### How do I get set up? ###

Source code can be found in `src/main/java` and tests can be found in `src/test/java`

For local development, the following was used (Java 8 required):

```
Apache Maven 3.2.5 (12a6b3acb947671f09b81f49094c53f426d8cea1; 2014-12-14T11:29:23-06:00)
Maven home: /usr/local/Cellar/maven/3.2.5/libexec
Java version: 1.8.0_31, vendor: Oracle Corporation
Java home: /Library/Java/JavaVirtualMachines/jdk1.8.0_31.jdk/Contents/Home/jre
Default locale: en_US, platform encoding: UTF-8
OS name: "mac os x", version: "10.11.6", arch: "x86_64", family: "mac"
```

To build, execute `mvn clean install` in the root directory.  This will generate a `target` folder. This is where the war file gets built by the maven war plugin.  The war file can then be run on an application server of your choosing.  Currently I have it deployed to AWS.  You can try it out as such:

`curl http://ztlapp.us-west-2.elasticbeanstalk.com/api/v1/parse -X POST --data-raw '@zac here. have you (eyes) this? http://blog.trello.com/trello-atlassian' -H 'Content-Type: text/plain'`

### Improvements ###

I think the basic functionality is there, but there are quite a few things I would like to add in order to make this a proper, reliable service.

* Increasing the unit test coverage. There are some corner cases, and code branches that I am missing
* Integration tests. I would bring in the cargo plugin and the failsafe plugin to automate integration tests against a running application server.
* Automated regression tests
* code coverage tests/enforcement using jacoco plugin
* Additional APIs - ideally, a proper service would have some sort of health check endpoint and a version info endpoint for troubleshooting
* Load/Performance testing. Test cases where messages have many URLs (requiring many network requests)
* Additional logging and better error handling/reporting.  Right now, uncaught errors will trickle up without providing useful information to the user.
* Not sure what the intended use case is for this service, but a batch upload endpoint maybe useful to prevent frequent network requests
* Swagger document REST API
* Automated deployment pipeline to AWS