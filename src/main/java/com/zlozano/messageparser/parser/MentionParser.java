package com.zlozano.messageparser.parser;

import com.zlozano.messageparser.model.MessageContent;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

/**
 * Specialized parser for finding/extracting mentions
 */
@Component
public class MentionParser extends MessageParser {

    public MentionParser() {
        p = Pattern.compile("(@\\w+)");
    }

    @Override
    void addParsedContent(String parsedContent, MessageContent messageContent) {
        // substring to strip @ sign
        messageContent.addMention(parsedContent.substring(1));
    }
}
