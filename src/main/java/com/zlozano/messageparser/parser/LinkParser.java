package com.zlozano.messageparser.parser;

import com.zlozano.messageparser.model.MessageContent;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 * Specialized parser for finding/extracting links
 */
@Component
public class LinkParser extends MessageParser {
    private static final Logger logger = Logger.getLogger(LinkParser.class.getName());

    public LinkParser() {
        // adapted from http://stackoverflow.com/questions/163360/regular-expression-to-match-urls-in-java
        p = Pattern.compile("((?:https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]*)+");
    }

    @Override
    void addParsedContent(String parsedContent, MessageContent messageContent) {
        try {
            messageContent.addLink(new URL(parsedContent), extractTitle(parsedContent));
        } catch (MalformedURLException e) {
            throw new IllegalStateException("Link parser parsed an invalid web link.", e);
        }
    }

    private String extractTitle(String url) {
        // If there is either no <title> tag, or we are unable to extract it, use URL as title.
        // This is what a web browser would do for the title shown on the tab.
        String value = url;
        try {
            Connection connection = getConnection(url);
            Document doc = connection.get();
            String title = doc.title();
            if (title != null && !title.isEmpty()) {
                value = title;
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Unable to establish connection", e);
        }
        return value;
    }

    // protected for unit testing.  not a huge fan of doing this, but it is way eaiser than trying to mock out a static method
    Connection getConnection(String url) {
        return Jsoup.connect(url);
    }
}
