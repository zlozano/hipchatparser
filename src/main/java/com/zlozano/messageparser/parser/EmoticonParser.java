package com.zlozano.messageparser.parser;

import com.zlozano.messageparser.model.MessageContent;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

/**
 * Specialized parser for finding/extracting emoticons
 */
@Component
public class EmoticonParser extends MessageParser {

    public EmoticonParser() {
        p = Pattern.compile("(\\([a-zA-Z0-9]+\\))");
    }

    @Override
    void addParsedContent(String parsedContent, MessageContent messageContent) {
        // substring to get rid of parens
        messageContent.addEmoticon(parsedContent.substring(1, parsedContent.length() - 1));
    }
}
