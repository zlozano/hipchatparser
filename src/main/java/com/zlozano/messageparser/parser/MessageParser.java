package com.zlozano.messageparser.parser;

import com.zlozano.messageparser.model.MessageContent;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class MessageParser {

    protected Pattern p;

    /**
     *
     * @param input input text containing emoticons, mentions, and links
     * @param messageContent parsed content model in which parsed content is collected (out parameter)
     */
    public void parseMessage(String input, MessageContent messageContent) {
        Matcher m = p.matcher(input);
        while (m.find()) {
            if (m.groupCount() == 1) {
                addParsedContent(m.group(1), messageContent);
            }
        }
    }

    /**
     * Template method to be implemented by specialized class
     * @param parsedContent the content found by pattern, p
     * @param messageContent parsed content model in which parsed content is collected (out parameter)
     */
    abstract void addParsedContent(String parsedContent, MessageContent messageContent);
}
