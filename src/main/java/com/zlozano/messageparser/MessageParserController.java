package com.zlozano.messageparser;

import com.zlozano.messageparser.model.MessageContent;
import com.zlozano.messageparser.parser.MessageParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/v1")
public class MessageParserController {

    @Autowired
    private List<MessageParser> parsers;

    @RequestMapping(
            value = "/parse",
            method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MessageContent> parseMessage(@RequestBody final String message) {
        if (message == null || message.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        final MessageContent content = new MessageContent();
        parsers.forEach(parser -> parser.parseMessage(message, content));
        return new ResponseEntity<>(content, HttpStatus.OK);
    }
}
