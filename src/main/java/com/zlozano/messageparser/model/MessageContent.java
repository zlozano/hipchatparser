package com.zlozano.messageparser.model;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MessageContent {
    private List<String> emoticons = new ArrayList<>();
    private List<String> mentions = new ArrayList<>();
    private List<Link> links = new ArrayList<>();

    public List<String> getEmoticons() {
        return emoticons;
    }

    public void setEmoticons(List<String> emoticons) {
        this.emoticons = emoticons;
    }

    public List<String> getMentions() {
        return mentions;
    }

    public void setMentions(List<String> mentions) {
        this.mentions = mentions;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }

    public void addMention(String mention) {
        if (mention != null) {
            mentions.add(mention);
        }
    }

    public void addEmoticon(String emoticon) {
        if (emoticon != null) {
            emoticons.add(emoticon);
        }
    }

    public void addLink(URL url, String title) {
        if (url != null && title != null) {
            links.add(new Link(url, title));
        }
    }
}
