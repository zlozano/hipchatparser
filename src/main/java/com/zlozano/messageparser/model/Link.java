package com.zlozano.messageparser.model;

import java.net.URL;

public class Link {
    private URL url; // using URL instead of String to provide some additional validation
    private String title;

    Link(URL url, String title) {
        this.url = url;
        this.title = title;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
