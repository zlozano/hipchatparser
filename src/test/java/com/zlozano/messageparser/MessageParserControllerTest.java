package com.zlozano.messageparser;

import com.zlozano.messageparser.model.MessageContent;
import com.zlozano.messageparser.parser.MessageParser;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by zlozano on 3/1/17.
 */
public class MessageParserControllerTest {

    @Spy
    private List<MessageParser> parsers;

    @InjectMocks
    private MessageParserController objectUnderTest;

    private MessageParser mockParser;
    private MockMvc mockMvc;

    @Before
    public void setup() {
        parsers = new ArrayList<>();
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(objectUnderTest).build();
        mockParser = Mockito.mock(MessageParser.class);
        parsers.add(mockParser);
    }

    @Test
    public void testPOSTGoldenPath() throws Exception {
        final String message = "@zac have you (eyes) this? http://google.com";

        Mockito.doAnswer(invocationOnMock -> {
            MessageContent content = (MessageContent)invocationOnMock.getArguments()[1];
            content.addEmoticon("eyes");
            content.addMention("zac");
            content.addLink(new URL("http://google.com"), "Some Webpage Title");
            return null;
        }).when(mockParser).parseMessage(Mockito.eq(message), Mockito.any(MessageContent.class));

        mockMvc.perform(post("/v1/parse")
                .content(message)
                .contentType(MediaType.TEXT_PLAIN))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.mentions[0]", is("zac")))
                .andExpect(jsonPath("$.emoticons[0]", is("eyes")))
                .andExpect(jsonPath("$.links[0].url", is("http://google.com")))
                .andExpect(jsonPath("$.links[0].title", is("Some Webpage Title")));
    }

    @Test
    public void testPOSTBadRequest() throws Exception {
        final String message = "";

        mockMvc.perform(post("/v1/parse")
                .content(message)
                .contentType(MediaType.TEXT_PLAIN))
                .andExpect(status().isBadRequest());
    }

}