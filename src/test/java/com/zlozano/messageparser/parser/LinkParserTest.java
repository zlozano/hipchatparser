package com.zlozano.messageparser.parser;

import com.zlozano.messageparser.model.Link;
import com.zlozano.messageparser.model.MessageContent;
import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class LinkParserTest {

    private LinkParser objectUnderTest;

    @Before
    public void setup() {
        objectUnderTest = Mockito.spy(new LinkParser());
    }

    @Test
    public void testExtractGoldenPath() throws Exception {
        final MessageContent messageContent = new MessageContent();
        final String url = "http://www.nbcolympics.com";
        final String input = "Olympics are starting soon; " + url;

        Connection mockConnection = Mockito.mock(Connection.class);
        Document mockDoc = Mockito.mock(Document.class);
        Mockito.when(objectUnderTest.getConnection(url)).thenReturn(mockConnection);
        Mockito.when(mockConnection.get()).thenReturn(mockDoc);
        Mockito.when(mockDoc.title()).thenReturn("Rio Olympics");

        objectUnderTest.parseMessage(input, messageContent);

        List<Link> links = messageContent.getLinks();
        assertEquals(1, links.size());
        assertEquals(new URL(url), links.get(0).getUrl());
        assertEquals("Rio Olympics", links.get(0).getTitle());
    }

    @Test
    public void testExtractGoldenPathNoTitleEmpty() throws Exception {
        final MessageContent messageContent = new MessageContent();
        final String url = "http://www.nbcolympics.com";
        final String input = "Olympics are starting soon; " + url;

        Connection mockConnection = Mockito.mock(Connection.class);
        Document mockDoc = Mockito.mock(Document.class);
        Mockito.when(objectUnderTest.getConnection(url)).thenReturn(mockConnection);
        Mockito.when(mockConnection.get()).thenReturn(mockDoc);
        Mockito.when(mockDoc.title()).thenReturn("");

        objectUnderTest.parseMessage(input, messageContent);

        List<Link> links = messageContent.getLinks();
        assertEquals(1, links.size());
        assertEquals(new URL(url), links.get(0).getUrl());
        assertEquals(url, links.get(0).getTitle());
    }

    @Test
    public void testExtractGoldenPathNoTitleNull() throws Exception {
        final MessageContent messageContent = new MessageContent();
        final String url = "http://www.nbcolympics.com";
        final String input = "Olympics are starting soon; " + url;

        Connection mockConnection = Mockito.mock(Connection.class);
        Document mockDoc = Mockito.mock(Document.class);
        Mockito.when(objectUnderTest.getConnection(url)).thenReturn(mockConnection);
        Mockito.when(mockConnection.get()).thenReturn(mockDoc);
        Mockito.when(mockDoc.title()).thenReturn(null);

        objectUnderTest.parseMessage(input, messageContent);

        List<Link> links = messageContent.getLinks();
        assertEquals(1, links.size());
        assertEquals(new URL(url), links.get(0).getUrl());
        assertEquals(url, links.get(0).getTitle());
    }

    @Test
    public void testExtractGoldenPathNoTitleIOException() throws Exception {
        final MessageContent messageContent = new MessageContent();
        final String url = "http://www.nbcolympics.com";
        final String input = "Olympics are starting soon; " + url;

        Connection mockConnection = Mockito.mock(Connection.class);
        Mockito.when(objectUnderTest.getConnection(url)).thenReturn(mockConnection);
        Mockito.doThrow(new IOException()).when(mockConnection).get();

        objectUnderTest.parseMessage(input, messageContent);

        List<Link> links = messageContent.getLinks();
        assertEquals(1, links.size());
        assertEquals(new URL(url), links.get(0).getUrl());
        assertEquals(url, links.get(0).getTitle());
    }

    @Test
    public void testExtractNone() throws Exception {
        final MessageContent messageContent = new MessageContent();
        final String input = "Olympics are starting soon; I couldn't find the link";

        objectUnderTest.parseMessage(input, messageContent);

        assertEquals(0, messageContent.getLinks().size());
    }
}