package com.zlozano.messageparser.parser;

import com.zlozano.messageparser.model.MessageContent;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class MentionParserTest {

    private MentionParser objectUnderTest = new MentionParser();

    @Test
    public void testExtractGoldenPath() {
        final MessageContent messageContent = new MessageContent();
        final String input = "@chris you around? @zac has a question";
        objectUnderTest.parseMessage(input, messageContent);

        List<String> mentions = messageContent.getMentions();
        assertEquals(2, mentions.size());
        assertEquals("chris", mentions.get(0));
        assertEquals("zac", mentions.get(1));
    }

    @Test
    public void testExtractNone() {
        final MessageContent messageContent = new MessageContent();
        final String input = "There are no mentions in this message";
        objectUnderTest.parseMessage(input, messageContent);

        assertEquals(0, messageContent.getMentions().size());
    }
}