package com.zlozano.messageparser.parser;

import com.zlozano.messageparser.model.MessageContent;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class EmoticonParserTest {

    private EmoticonParser objectUnderTest = new EmoticonParser();

    @Test
    public void testExtractGoldenPath0() {
        final MessageContent messageContent = new MessageContent();
        final String input = "Good morning! (megusta) (coffee)";
        objectUnderTest.parseMessage(input, messageContent);

        List<String> emoticons = messageContent.getEmoticons();
        assertEquals(2, emoticons.size());
        assertEquals("megusta", emoticons.get(0));
        assertEquals("coffee", emoticons.get(1));
    }

    @Test
    public void testExtractGoldenPath1() {
        final MessageContent messageContent = new MessageContent();
        final String input = "Good morning! (megusta)(coffee)"; // no space
        objectUnderTest.parseMessage(input, messageContent);

        List<String> emoticons = messageContent.getEmoticons();
        assertEquals(2, emoticons.size());
        assertEquals("megusta", emoticons.get(0));
        assertEquals("coffee", emoticons.get(1));
    }

    @Test
    public void testExtractNone() {
        final MessageContent messageContent = new MessageContent();
        final String input = "Boring message with no emoticons.";
        objectUnderTest.parseMessage(input, messageContent);

        assertEquals(0, messageContent.getEmoticons().size());
    }

}